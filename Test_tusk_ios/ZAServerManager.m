//
//  ZAServerManager.m
//  Test_tusk_ios
//
//  Created by Macbook on 2/9/15.
//  Copyright (c) 2015 Macbook. All rights reserved.
//

#import "ZAServerManager.h"
#import "ZADataForCell.h"

@interface ZAServerManager ()

@property (strong, nonatomic) AFHTTPRequestOperationManager *requestOperationManager;

@end

@implementation ZAServerManager

+ (ZAServerManager *) sharedManager {
    
    static ZAServerManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZAServerManager alloc] init];
    });
    
    return manager;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        
        NSString *urlString = @"http://api.vk.com/method";
        
        NSURL *url = [NSURL URLWithString:urlString];
        
        self.requestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    }
    
    return self;
}

- (void) getRecordWithOffset:(NSInteger) offset
                       count:(NSInteger) count
                   onSuccess:(void(^)(NSArray *dataCell)) success
                   onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"-30666517",@"owner_id",
                            @(count),@"count",
                            @(offset),@"offset",
                            @(0),@"extended",
                            @(5.28),@"v",nil];
    
    [self.requestOperationManager
     GET:@"wall.get"
     parameters:params
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"JSON: %@",responseObject);
         
         NSArray *dictArray = [[responseObject objectForKey:@"response"] objectForKey:@"items"];
         
         NSMutableArray *objectsArray = [NSMutableArray array];
         
         for (NSDictionary *dict in dictArray) {
        
             ZADataForCell *dataCell = [[ZADataForCell alloc] initWithInfo:dict];
             [objectsArray addObject:dataCell];

         }
         
         if (success) {
             success(objectsArray);
         }
         
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         if (failure) {
             
             failure(error,operation.response.statusCode);
             
         }
         
     }];
    
}

@end
