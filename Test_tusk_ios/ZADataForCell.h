//
//  ZADataForCell.h
//  Test_tusk_ios
//
//  Created by Macbook on 2/9/15.
//  Copyright (c) 2015 Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZADataForCell : NSObject

@property (strong, nonatomic) NSString *descriptionText;
@property (assign, nonatomic) NSTimeInterval date;

- (instancetype) initWithInfo:(NSDictionary *) info;

@end
