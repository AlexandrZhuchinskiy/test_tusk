//
//  ViewController.m
//  Test_tusk_ios
//
//  Created by Macbook on 2/9/15.
//  Copyright (c) 2015 Macbook. All rights reserved.
//
#import "ZAServerManager.h"
#import "ZAShowDataViewController.h"
#import "ZADataForCell.h"
#import "ZACellForShow.h"

@interface ZAShowDataViewController ()

@property (strong, nonatomic) NSArray *arrayDataCell;

@end

@implementation ZAShowDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    [[ZAServerManager sharedManager] getRecordWithOffset:0
                                                   count:10
                                               onSuccess:^(NSArray *dataCell) {
                                                   self.arrayDataCell = [NSArray arrayWithArray:dataCell];
                                                   [self.tableView reloadData];
                                               }
                                               onFailure:^(NSError *error, NSInteger statusCode) {
                                                   
                                               }];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    
    return [self.arrayDataCell count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    
    ZACellForShow *cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell"];
    ZADataForCell *dataForCell = [self.arrayDataCell objectAtIndex:indexPath.row];
    
    cell.descriptionTextOutlet.text = dataForCell.descriptionText;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:dataForCell.date];
    
    cell.dateLabelOutlet.text = [self stringfromDate:date];
    
    return cell;
}

#pragma mark - stringFromDate

- (NSString *) stringfromDate:(NSDate *) date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

@end
