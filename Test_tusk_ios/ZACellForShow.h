//
//  ZACellForShow.h
//  Test_tusk_ios
//
//  Created by Macbook on 2/9/15.
//  Copyright (c) 2015 Macbook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZACellForShow : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *descriptionTextOutlet;

@end
