//
//  ZAServerManager.h
//  Test_tusk_ios
//
//  Created by Macbook on 2/9/15.
//  Copyright (c) 2015 Macbook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface ZAServerManager : NSObject

+ (ZAServerManager *) sharedManager;

- (void) getRecordWithOffset:(NSInteger) offset
                       count:(NSInteger) count
                   onSuccess:(void(^)(NSArray *dataCell)) success
                   onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

@end
