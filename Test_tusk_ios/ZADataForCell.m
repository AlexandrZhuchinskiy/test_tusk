//
//  ZADataForCell.m
//  Test_tusk_ios
//
//  Created by Macbook on 2/9/15.
//  Copyright (c) 2015 Macbook. All rights reserved.
//

#import "ZADataForCell.h"

@implementation ZADataForCell

- (instancetype) initWithInfo:(NSDictionary *) info {
    
    self = [super init];
    
    if (self) {
        self.date = [[info objectForKey:@"date"] doubleValue];
        self.descriptionText = [info objectForKey:@"text"];
    }
    
    return self;
}
@end
